package com.timmciver.wordscapessolverweb

import cats.effect._
import cats.implicits._

import org.http4s.implicits._
import org.http4s._
import org.http4s.server.blaze._
import org.http4s.server.blaze.BlazeBuilder

import scala.io.Source

object WordscapesServer extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    ServerStream.stream[IO].compile.drain.as(ExitCode.Success)
}

object ServerStream {

  def service[F[_]: Effect] = new WordscapesService[F].service

  def stream[F[_]: ConcurrentEffect : Timer]
    = BlazeBuilder[F]
      .bindHttp(8080, "0.0.0.0")
      .mountService(service, "/")
      .serve
}
