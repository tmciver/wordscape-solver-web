package com.timmciver.wordscapessolverweb.view

import scalatags.Text.all._
import org.http4s.MediaType.text

object WordscapesSolverViews {

  def homeView: String = html(
    head(
      script("some script")
    ),
    body(
      h1("Wordscapes Solver"),
      form(action:="/solver", method:="GET")(
        div(
          label("Enter puzzle letters:"),
          input(`type`:="text", name:="letters", id:="letters")
        ),
        div(
          button(`type`:="submit")("Submit")
        )
      )
    )
  ).toString

  def solutionView(letters: String, solution: Set[String]): String = html(
    head(
      script("some script")
    ),
    body(
      h1("Wordscapes Solver"),
      p("Possible words from letters \"" + letters + "\" are: " + solution.mkString(", ")),
      a(href:="/")("Home")
    )
  ).toString
}
