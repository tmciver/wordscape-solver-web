package com.timmciver.wordscapessolverweb

import cats.effect._
import cats.syntax.functor._
import cats.syntax.flatMap._

import org.http4s._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers._
import org.http4s.MediaType.text

import com.timmciver.wordscapessolver.WordscapesSolver
import com.timmciver.wordscapessolverweb.view.WordscapesSolverViews

class WordscapesService[F[_]: Effect] extends Http4sDsl[F] {

  object LettersQueryParamMatcher extends QueryParamDecoderMatcher[String]("letters")

  val service = {
    HttpRoutes.of[F] {
      case GET -> Root => Ok(WordscapesSolverViews.homeView, `Content-Type`(MediaType.text.html))
      case GET -> Root / "solver" :? LettersQueryParamMatcher(letters) => {
        WordscapesSolver.solve(letters)
          .map(WordscapesSolverViews.solutionView(letters, _))
          .flatMap(Ok(_, `Content-Type`(MediaType.text.html)))
      }
    }
  }
}
