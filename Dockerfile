FROM openjdk:8

COPY target/scala-2.12/wordscapes-solver-web-assembly-0.0.1-SNAPSHOT.jar .
CMD java -jar wordscapes-solver-web-assembly-0.0.1-SNAPSHOT.jar
